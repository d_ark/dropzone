function SelectForm(form)
{
	if (form.SelectCountType.options.selectedIndex == 0)
	{
		document.getElementById("BeginDivCount").className = "ActiveFormCount";
		form.JumpNumber.required = true;
		document.getElementById("SportDivCount").className = "BackFormCount";
	}
	else
	{
		document.getElementById("BeginDivCount").className = "BackFormCount";
		form.JumpNumber.required = false;
		document.getElementById("SportDivCount").className = "ActiveFormCount";
	}
}

function TypeSelectorChange(form)
{
	form.weight90.disabled = (form.TypeSelector.options.selectedIndex != 1);
}

function ParashuteChange(chbox, form)
{
	form.ParType[0].disabled = !chbox.checked;
	form.ParType[1].disabled = !chbox.checked;
	form.Ukladka.disabled = chbox.checked;
}

function InitPrices()
{
	SelectForm(document.forms.Counter);
}

function CheckIfNum(input)
{
	var value = input.value;
	var val="";
	var b = false;
	for (i = 0, n = (value.length>10)?10:value.length; i < n; ++i)
	{
		if ('0' <= value[i] && value[i] <= '9' && (b || value[i] != '0'))
			val += value[i];
			b = b || value[i] != 0;
	}
	input.value = val;
}

function CountPrice(form)
{
	if (document.getElementById("BeginDivCount").className == "ActiveFormCount")
	{	// Beginers
		var p = form.TypeSelector.options.selectedIndex;
		if (p == 0)
		{
			if (form.JumpNumber.value == "1") return "360.00";
			if (form.JumpNumber.value == "2") return "297.00";
			if (form.JumpNumber.value == "3") return "270.00";
			return "252.00";
		}
		if (p == 1)
		{
			if (form.weight90.checked) return "1080.00";
			else return "990.00"
		}
		if (p == 2)
		{
			if (form.JumpNumber.value == "1") return "333.00";
			if (form.JumpNumber.value == "2") return "252.00";
			if (form.JumpNumber.value == "3") return "243.00";
			return "225.00";
		}
		if (p == 3)
		{
			if (form.JumpNumber.value == "1") return "1080.00";
			if (form.JumpNumber.value == "2") return "900.00";
			return "855.00";
		}
		if (p == 10)
		{
			return "360.00";
		}
		if (p == 11)
		{
			return "540.00";
		}
		var a = [900,810,720,720,630,540];
		p -= 3;
		if (form.JumpNumber.value == "1")
			return a[p] + ".00";
		return (a[p] - 45) + ".00";
	}
	else
	{	//Sports
		var p;
		if (form.HeightSelector.options.selectedIndex == 0) p = 90;
		if (form.HeightSelector.options.selectedIndex == 1) p = 150;
		if (form.HeightSelector.options.selectedIndex == 2) p = 180;
		
		if (form.Altimeter.checked) p += 9;
		if (form.Combineson.checked) p += 9;
		if (form.Oculars.checked) p += 9;
		if (form.Helmet.checked) p += 9;
		
		if (form.Parashute.checked)
		{
			if (form.ParType[0].checked) p += 77;
			else p += 54;
		}
		else
		{
			if (form.Ukladka.checked)
				p += 23;
		}
	}
	return p + ".00";
}

function Submition(form)
{
	alert("������ ��� ��� ����� ������ " + CountPrice(form) + " ������. ���� ��� �� ��������!");
}