function ShowPicture()
{
	document.getElementById("HiddenPic").src = this.src;
	document.getElementById("picturelay").style.visibility="visible";
}

function HidePicture()
{
	document.getElementById("picturelay").style.visibility="hidden";
}

function InitPictureView()
{
	ls = document.getElementsByClassName("leftsmall");
	for (i = 0, n = ls.length; i < n; ++i)
		ls[i].onclick = ShowPicture;
	ls = document.getElementsByClassName("rightsmall");
	for (i = 0, n = ls.length; i < n; ++i)
		ls[i].onclick = ShowPicture;
	document.getElementById("picturelay").onclick = HidePicture;
}